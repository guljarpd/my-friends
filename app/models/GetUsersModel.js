const PG_DB = require('../services/DbConnection');

// get users fromm DB with pagination
const getUserListModel = async (limit, offset) => {
  try {
    const queryText = 'SELECT * FROM users ORDER BY id DESC LIMIT $1 OFFSET $2';
    const values = [limit, offset];
    // execute SQL query.
    const result = await PG_DB.query(queryText, values);
    console.log(result.rows);
    return result.rows;
  } catch (e) {
    console.error('Error getUserListModel', e);
    return null;
  }
}

//
module.exports = {
  getUserListModel
}
