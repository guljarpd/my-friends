const PG_DB = require('../services/DbConnection');

// get users fromm DB with pagination
const getFriendsListModel = async (userId) => {
  try {
    const queryText = `
      SELECT U1.id, U1.firstname, U1.lastname, U1.avatar, U1.created_at from
      users AS U1
      RIGHT JOIN my_friends AS MF
      ON  U1.id = MF.my_friend_id
      WHERE MF.user_id = $1
    `
    const values = [userId];
    // execute SQL query.
    const result = await PG_DB.query(queryText, values);
    console.log(result.rows);
    return result.rows;
  } catch (e) {
    console.error('Error getFriendsListModel', e);
    return null;
  }
}

//
module.exports = {
  getFriendsListModel
}
