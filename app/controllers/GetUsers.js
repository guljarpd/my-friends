//
const { getUserListModel } = require('../models/GetUsersModel');

// get all users with pagination API.
const getUserList = async (req, res, next) => {
  try {
    let { pageNo } = req.params;
    //
    console.log(pageNo);
    if (!pageNo) {
      res.status(404).json({
        error: {
          message: 'Page number required.',
          code: 'PAGE_NO'
        }
      });
    }
    // convert string number to integer
    pageNo = parseInt(pageNo);
    //
    let limit = 10; // limit
    //
    let offset = (pageNo * 10) - 10; // calculating offset
    //
    let userList = await getUserListModel(limit, offset);
    // console.log('');
    if (userList) {
      res.status(200).json({
        data: userList
      })
    } else {
      res.status(404).json({
        error: {
          message: 'User data not found.',
          code: 'NOT_FOUND'
        }
      });
    }
  } catch (e) {
    console.error('Error getUserList', e);
    res.status(500).json({
      error: {
        message: 'Something went wrong.',
        code: 'SOMETHING_WENT_WRONG'
      }
    });
  }
}

//
module.exports = {
  getUserList
}
