//
const { getFriendsListModel } = require('../models/GetFriendsModel');

// get friends of user with user id.
const getFriendsList = async (req, res, next) => {
  try {
    //
    let { userId } = req.params;
    //
    console.log(userId);
    if (!userId) {
      res.status(404).json({
        error: {
          message: 'User id required.',
          code: 'USER_ID'
        }
      });
    }
    // convert string number to integer
    userId = parseInt(userId);
    //
    let userList = await getFriendsListModel(userId);
    // console.log('');
    if (userList) {
      res.status(200).json({
        data: userList
      })
    } else {
      res.status(404).json({
        error: {
          message: 'Users data not found',
          code: 'NOT_FOUND'
        }
      });
    }
  } catch (e) {
    console.error('Error getFriendsList', e);
    res.status(500).json({
      error: {
        message: 'Something went wrong.',
        code: 'SOMETHING_WENT_WRONG'
      }
    });
  }
}

//
module.exports = {
  getFriendsList
}
