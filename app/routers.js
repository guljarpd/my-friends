module.exports = function(app) {
  const version = [1,2,3];
  // include all controller and middleware files here
  // version [v1]
  const GetUsers = require('./controllers/GetUsers');
  const GetFriends = require('./controllers/GetFriends');

  // version [v2]
  // version [v3]
  // ...



  // make API endpoint
  // get user list
  app.get(`/api/v${version[0]}/users/list/:pageNo`, GetUsers.getUserList);
  // get friends list
  app.get(`/api/v${version[0]}/friends/list/:userId`, GetFriends.getFriendsList);

}
