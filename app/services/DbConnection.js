const { Pool } = require('pg');
const config = require('config');
// postgreSQL DB connection.
const connectionQuery = {
  user: config.PG_DB.USERNAME,
  host: config.PG_DB.HOSTNAME,
  database: config.PG_DB.DATABASE,
  password: config.PG_DB.PASSWORD,
  port: config.PG_DB.PORT,
}
//
// console.log(connectionQuery);
//
const pool = new Pool(connectionQuery);
module.exports = pool;
