const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const should = chai.should();
//
chai.use(chaiHttp);

//
describe('/GET users', () => {
  it('it should GET users', (done) => {
    chai.request(server)
      .get('/api/v1/users/list/1')
      .end((err, res) => {
        // console.log(res.body.data);
        res.should.have.status(200);  //check status
        res.body.should.be.a('object'); // check data type
        res.body.data.length.should.be.eql(10); // check result count
        done();
      });
  });
});
