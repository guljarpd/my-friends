# Friends

Users and their friends project. Written in Nodejs with PostgreSQL Database.

Installation requirement.
#### Nodemon watchman package for development
`npm install -g nodemon`

#### Install all packages  
`npm install`

#### PostgreSQL DB Config.
Setup all required params in `config` folder.
#### Sample DB Schema file `db_schema/db.sql`
import `.sql` file in pgAdmin.
open `queryTool` navigate to `Open File`
Select backup file and execute.
This sample file will create Database, Table and insert all data into tables.

In the project directory, you can run:
##### For Development
`npm run dev`
##### For Test Case
`npm test`
##### For Production
`npm start`

#### Server running PORT `8080`
Base URL for localhost
##### BASE_URL: http://localhost:8080
Note:- you can replce `BASE_URL` with your Hostname.
### API Endpoints
Get user's list. Pagination API.
`/api/v1/users/list/{pageNo}`
###### Method `GET`
Where `pageNo` is the number of page which you have to view.
###### Response
```js
{
  data:[
    {
      id: 50,
      firstname: "Brandon",
      lastname: "Kelley",
      avatar: "https://robohash.org/BrandonKelley.jpg"
    }
 ]
}
```

Get user's Friends List.
`/api/v1/friends/list/{userId}`
###### Method `GET`
Where `userId` is the user id of user Or Friend user id.
###### Response
```js
{
  data:[
    {
      id: 30,
      firstname: "Prandon",
      lastname: "Oelley",
      avatar: "https://robohash.org/PrandonOelley.jpg"
    }
 ]
}
```
