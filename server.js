const express = require('express');
const app = express();
const cors = require('cors');
const http = require('http');
const bodyParser = require('body-parser');
//
console.log(process.env.NODE_ENV);
require('dotenv').config(process.env.NODE_ENV);
// console.log(process.env);
//
const config = require('config');
// skip cross origin checks.
app.use(cors());
// parse json body.
app.use(bodyParser.json({
  type: 'application/json'
}));
//
// include router file here. and route all request.
require('./app/routers')(app);
// app root endpoint
app.get('/', (req, res, next) => {
  console.log('root api call');
  res.status(200).json({
    data: 'Hello Friends! '
  });
});
//
// handle errors
app.use((req, res, next) => {
  // console.error(req);
  //
  res.status(500).json({
    error: 'Something went wrong.'
  });
});

// define server port
const port = config.PORT || 8080;
const server = http.createServer(app);
server.listen(port);
console.log('server is running', port);
module.exports = server;
