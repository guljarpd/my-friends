--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

-- Started on 2020-09-13 14:13:53 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2946 (class 1262 OID 16392)
-- Name: friends; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE friends WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_IN' LC_CTYPE = 'en_IN';


ALTER DATABASE friends OWNER TO postgres;

\connect friends

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 205 (class 1259 OID 16448)
-- Name: my_friends; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.my_friends (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    my_friend_id bigint NOT NULL
);


ALTER TABLE public.my_friends OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16446)
-- Name: my_friends_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.my_friends_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.my_friends_id_seq OWNER TO postgres;

--
-- TOC entry 2947 (class 0 OID 0)
-- Dependencies: 204
-- Name: my_friends_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.my_friends_id_seq OWNED BY public.my_friends.id;


--
-- TOC entry 203 (class 1259 OID 16433)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    firstname character varying(50) NOT NULL,
    lastname character varying(50) NOT NULL,
    avatar text,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16431)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2948 (class 0 OID 0)
-- Dependencies: 202
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2806 (class 2604 OID 16451)
-- Name: my_friends id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.my_friends ALTER COLUMN id SET DEFAULT nextval('public.my_friends_id_seq'::regclass);


--
-- TOC entry 2804 (class 2604 OID 16436)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2940 (class 0 OID 16448)
-- Dependencies: 205
-- Data for Name: my_friends; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.my_friends (id, user_id, my_friend_id) FROM stdin;
1	1	18
2	1	38
3	1	39
4	1	32
5	1	6
6	2	5
7	2	17
8	2	38
9	2	29
10	2	6
11	3	27
12	3	50
13	3	13
14	3	29
15	3	7
16	4	1
17	4	21
18	4	30
19	4	17
20	4	45
21	5	14
22	5	43
23	5	19
24	5	46
25	5	27
26	6	29
27	6	46
28	6	13
29	6	10
30	6	4
31	7	2
32	7	10
33	7	39
34	7	6
35	7	13
36	8	13
37	8	22
38	8	47
39	8	46
40	8	35
41	9	7
42	9	30
43	9	47
44	9	39
45	9	27
46	10	11
47	10	28
48	10	18
49	10	44
50	10	6
51	11	36
52	11	12
53	11	24
54	11	9
55	11	11
56	12	8
57	12	15
58	12	47
59	12	2
60	12	14
61	13	49
62	13	20
63	13	8
64	13	50
65	13	14
66	14	50
67	14	42
68	14	38
69	14	5
70	14	11
71	15	8
72	15	29
73	15	25
74	15	12
75	15	13
76	16	1
77	16	13
78	16	38
79	16	21
80	16	16
81	17	32
82	17	26
83	17	30
84	17	46
85	17	44
86	18	18
87	18	47
88	18	16
89	18	38
90	18	22
91	19	35
92	19	23
93	19	27
94	19	11
95	19	34
96	20	26
97	20	6
98	20	49
99	20	16
100	20	45
101	21	32
102	21	45
103	21	30
104	21	27
105	21	5
106	22	11
107	22	5
108	22	6
109	22	49
110	22	13
111	23	32
112	23	50
113	23	49
114	23	24
115	23	44
116	24	36
117	24	17
118	24	12
119	24	9
120	24	25
121	25	17
122	25	23
123	25	22
124	25	33
125	25	47
126	26	17
127	26	10
128	26	1
129	26	11
130	26	22
131	27	47
132	27	38
133	27	49
134	27	1
135	27	39
136	28	41
137	28	36
138	28	19
139	28	38
140	28	1
141	29	46
142	29	16
143	29	39
144	29	49
145	29	8
146	30	41
147	30	28
148	30	23
149	30	44
150	30	16
151	31	39
152	31	48
153	31	19
154	31	25
155	31	42
156	32	50
157	32	48
158	32	46
159	32	41
160	32	29
161	33	9
162	33	12
163	33	29
164	33	45
165	33	11
166	34	41
167	34	10
168	34	5
169	34	40
170	34	44
171	35	46
172	35	45
173	35	17
174	35	39
175	35	2
176	36	13
177	36	49
178	36	29
179	36	22
180	36	27
181	37	3
182	37	27
183	37	11
184	37	24
185	37	44
186	38	37
187	38	24
188	38	38
189	38	5
190	38	23
191	39	32
192	39	39
193	39	6
194	39	2
195	39	19
196	40	15
197	40	21
198	40	35
199	40	43
200	40	27
201	41	17
202	41	27
203	41	12
204	41	10
205	41	37
206	42	11
207	42	22
208	42	34
209	42	38
210	42	10
211	43	36
212	43	27
213	43	26
214	43	5
215	43	32
216	44	16
217	44	42
218	44	20
219	44	18
220	44	29
221	45	49
222	45	46
223	45	17
224	45	10
225	45	35
226	46	11
227	46	44
228	46	31
229	46	24
230	46	5
231	47	23
232	47	7
233	47	48
234	47	30
235	47	8
236	48	34
237	48	26
238	48	13
239	48	22
240	48	44
241	49	32
242	49	26
243	49	43
244	49	5
245	49	28
246	50	34
247	50	19
248	50	44
249	50	25
250	50	13
\.


--
-- TOC entry 2938 (class 0 OID 16433)
-- Dependencies: 203
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, firstname, lastname, avatar, created_at) FROM stdin;
1	Andrew	Diaz	https://robohash.org/AndrewDiaz.jpg	2020-09-12 18:09:37.835634+05:30
2	Michael	Pierce	https://robohash.org/MichaelPierce.jpg	2020-09-12 18:09:37.835634+05:30
3	Craig	Hanson	https://robohash.org/CraigHanson.jpg	2020-09-12 18:09:37.835634+05:30
4	Danielle	Ortiz	https://robohash.org/DanielleOrtiz.jpg	2020-09-12 18:09:37.835634+05:30
5	Nicole	Nichols	https://robohash.org/NicoleNichols.jpg	2020-09-12 18:09:37.835634+05:30
6	Richard	Baker	https://robohash.org/RichardBaker.jpg	2020-09-12 18:09:37.835634+05:30
7	Paula	Mccoy	https://robohash.org/PaulaMccoy.jpg	2020-09-12 18:09:37.835634+05:30
8	Kenneth	Ritter	https://robohash.org/KennethRitter.jpg	2020-09-12 18:09:37.835634+05:30
9	Michael	Thompson	https://robohash.org/MichaelThompson.jpg	2020-09-12 18:09:37.835634+05:30
10	Nichole	Weiss	https://robohash.org/NicholeWeiss.jpg	2020-09-12 18:09:37.835634+05:30
11	Nicholas	Villa	https://robohash.org/NicholasVilla.jpg	2020-09-12 18:09:37.835634+05:30
12	Megan	Fox	https://robohash.org/MeganFox.jpg	2020-09-12 18:09:37.835634+05:30
13	Annette	Austin	https://robohash.org/AnnetteAustin.jpg	2020-09-12 18:09:37.835634+05:30
14	Peter	Frank	https://robohash.org/PeterFrank.jpg	2020-09-12 18:09:37.835634+05:30
15	Christopher	Taylor	https://robohash.org/ChristopherTaylor.jpg	2020-09-12 18:09:37.835634+05:30
16	Samantha	Haynes	https://robohash.org/SamanthaHaynes.jpg	2020-09-12 18:09:37.835634+05:30
17	Misty	Rice	https://robohash.org/MistyRice.jpg	2020-09-12 18:09:37.835634+05:30
18	Ryan	Barron	https://robohash.org/RyanBarron.jpg	2020-09-12 18:09:37.835634+05:30
19	Kelly	Wilson	https://robohash.org/KellyWilson.jpg	2020-09-12 18:09:37.835634+05:30
20	Cheryl	Martin	https://robohash.org/CherylMartin.jpg	2020-09-12 18:09:37.835634+05:30
21	Leslie	Sanchez	https://robohash.org/LeslieSanchez.jpg	2020-09-12 18:09:37.835634+05:30
22	Lisa	Solis	https://robohash.org/LisaSolis.jpg	2020-09-12 18:09:37.835634+05:30
23	Julia	Hart	https://robohash.org/JuliaHart.jpg	2020-09-12 18:09:37.835634+05:30
24	Rodney	Johnson	https://robohash.org/RodneyJohnson.jpg	2020-09-12 18:09:37.835634+05:30
25	Douglas	Cervantes	https://robohash.org/DouglasCervantes.jpg	2020-09-12 18:09:37.835634+05:30
26	Colin	Williams	https://robohash.org/ColinWilliams.jpg	2020-09-12 18:09:37.835634+05:30
27	Rebecca	Ray	https://robohash.org/RebeccaRay.jpg	2020-09-12 18:09:37.835634+05:30
28	Thomas	Hicks	https://robohash.org/ThomasHicks.jpg	2020-09-12 18:09:37.835634+05:30
29	Tina	Crawford	https://robohash.org/TinaCrawford.jpg	2020-09-12 18:09:37.835634+05:30
30	Samantha	Kim	https://robohash.org/SamanthaKim.jpg	2020-09-12 18:09:37.835634+05:30
31	Leah	Garrison	https://robohash.org/LeahGarrison.jpg	2020-09-12 18:09:37.835634+05:30
32	Angela	Daniel	https://robohash.org/AngelaDaniel.jpg	2020-09-12 18:09:37.835634+05:30
33	Katherine	Scott	https://robohash.org/KatherineScott.jpg	2020-09-12 18:09:37.835634+05:30
34	Angela	Higgins	https://robohash.org/AngelaHiggins.jpg	2020-09-12 18:09:37.835634+05:30
35	Michelle	Robinson	https://robohash.org/MichelleRobinson.jpg	2020-09-12 18:09:37.835634+05:30
36	Daniel	Jones	https://robohash.org/DanielJones.jpg	2020-09-12 18:09:37.835634+05:30
37	William	Sanders	https://robohash.org/WilliamSanders.jpg	2020-09-12 18:09:37.835634+05:30
38	Sara	Hines	https://robohash.org/SaraHines.jpg	2020-09-12 18:09:37.835634+05:30
39	Christine	Sloan	https://robohash.org/ChristineSloan.jpg	2020-09-12 18:09:37.835634+05:30
40	Arthur	Chavez	https://robohash.org/ArthurChavez.jpg	2020-09-12 18:09:37.835634+05:30
41	Cynthia	Hayden	https://robohash.org/CynthiaHayden.jpg	2020-09-12 18:09:37.835634+05:30
42	Daniel	Lopez	https://robohash.org/DanielLopez.jpg	2020-09-12 18:09:37.835634+05:30
43	Robert	Richards	https://robohash.org/RobertRichards.jpg	2020-09-12 18:09:37.835634+05:30
44	Mary	Cook	https://robohash.org/MaryCook.jpg	2020-09-12 18:09:37.835634+05:30
45	Christopher	Horton	https://robohash.org/ChristopherHorton.jpg	2020-09-12 18:09:37.835634+05:30
46	Jon	Goodwin	https://robohash.org/JonGoodwin.jpg	2020-09-12 18:09:37.835634+05:30
47	Robert	King	https://robohash.org/RobertKing.jpg	2020-09-12 18:09:37.835634+05:30
48	Danny	Rich	https://robohash.org/DannyRich.jpg	2020-09-12 18:09:37.835634+05:30
49	Teresa	Schmidt	https://robohash.org/TeresaSchmidt.jpg	2020-09-12 18:09:37.835634+05:30
50	Brandon	Kelley	https://robohash.org/BrandonKelley.jpg	2020-09-12 18:09:37.835634+05:30
\.


--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 204
-- Name: my_friends_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.my_friends_id_seq', 250, true);


--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 202
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 50, true);


--
-- TOC entry 2810 (class 2606 OID 16453)
-- Name: my_friends my_friends_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.my_friends
    ADD CONSTRAINT my_friends_pkey PRIMARY KEY (id);


--
-- TOC entry 2808 (class 2606 OID 16442)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


-- Completed on 2020-09-13 14:13:54 IST

--
-- PostgreSQL database dump complete
--

